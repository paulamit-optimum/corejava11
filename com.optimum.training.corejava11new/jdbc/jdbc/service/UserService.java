package jdbc.service;

public interface UserService {
	
	void deleteRecord();
	void updateUserRecord();
	void getUserRecord();
	void userInputInsertRecord();
	void userChoice();
	void getUserByID();
}
