package day24;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

// manual test ==> JUnit 4 and JUnit 5

public class Example91 {
	
	@BeforeClass
	public static void testCase1() {
		new DeveloperCode().method1();
	}
	
	@AfterClass
	public static void testCase2() {
		new DeveloperCode().method2();
	}
	
	@Before
	public void testCase3() {
		new DeveloperCode().method3();
	}
	
	@After
	public void testCase4() {
		new DeveloperCode().method4();
	}
	
	@Test
	public void testCase5() {
		new DeveloperCode().method5();
	}
}
