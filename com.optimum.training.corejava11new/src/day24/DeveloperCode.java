package day24;


public class DeveloperCode {
	
	void method1() {
		System.out.println("inside method-1");
	}
	
	void method2() {
		System.out.println("inside method-2");
	}
	
	void method3() {
		System.out.println("inside method-3");
	}
	
	void method4() {
		System.out.println("inside method-4");
	}

	void method5() {
		System.out.println("inside-5 --> main()");
	}

}
