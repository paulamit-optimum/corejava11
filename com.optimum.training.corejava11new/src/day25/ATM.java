package day25;

public interface ATM {
	
	double deposit(double amount);
	double withdrawal(double amount);
	double checkBalanace();

}
