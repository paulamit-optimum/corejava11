package day25;

import java.util.Scanner;

public class ATMImpl implements ATM{

	Scanner refScanner = null;
	double availableBalance = 0;
	
	@Override
	public double deposit(double amount) {
		availableBalance = availableBalance +amount;
		return availableBalance;
	}

	@Override
	public double withdrawal(double amount) {
		availableBalance = availableBalance - amount;
		return availableBalance;
	}

	@Override
	public double checkBalanace() {
		return availableBalance;
	}

	void userInput() {
		while(true) {
		refScanner = new Scanner("Enter your choice : ");
		int choice = refScanner.nextInt();
		userChoice(choice);
		}
	} // end of userInput
	
	void userChoice(int choice) {
		switch (choice) {
		case 1:
			System.out.println("Enter amount for the deposit : ");
			int depositAmount = refScanner.nextInt();
			deposit(depositAmount);
			break;

		case 2:
			System.out.println("Enter amount for the withdrawal : ");
			int withdrawalAmount = refScanner.nextInt();
			withdrawal(withdrawalAmount);
			break;

		case 3:
			System.out.println("Check your balance : ");
			System.out.println(checkBalanace());
			break;
	
		default:
			break;
		}
	}
	
}
