package day25;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

// Test Scenarios	ATM DWC	
//		Test Case1	@Test
//		Test Case2
//		Test Case3

//		and so on


public class Example92 {
	
	static ATMImpl ref;
	
	@BeforeClass
	public static void testCase1() {
		System.out.println("Running Before Class");
		ref = new ATMImpl();
		ref.userChoice(3);  					// 3==> checkBalance
		assertTrue(ref.checkBalanace()==0);
	}
	
	@Before
	public void testCase2() {
		System.out.println("\nAfter deposit available balance amount :" +ref.deposit(20.50));
	}
	
	@Test
	public void testCase3() {
		ref.userChoice(3);  					// 3==> checkBalance
	}
	
	@After
	public void testCase4() {
		System.out.println("\nAfter withdrawal available balanace amount :"+ref.withdrawal(15.20));
	}
	
	@AfterClass
	public static void testCase5() {
		System.out.println("Running After Class");
		ref.userChoice(3);  					// 3==> checkBalance  5.30
		assertEquals(false,ref.checkBalanace()>6);
	}
}
