package day15;

class Demo10{
	
	{
		new Demo11();
	}
}

class Demo11{
	
	static{
		new Demo12();
	}
}

class Demo12{
	Demo12(){
		System.out.println("hello");
	}
}

public class Example55 {

	public static void main(String[] args) {
		
		new Demo10();

	}

}
