package day20;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Example69 {

	public static void main(String[] args) {
		
		List refList = new ArrayList();		// up casting
		
		// we are adding value to the list
		
		refList.add("Hello"); 	// String type
		refList.add(12345);		// int type
		refList.add(true);		// boolean type
		
		if (refList.contains("Hello")) {
			System.out.println("Hello - object found..");
		} else {
			System.out.println("Hello - object didn't found");
		}
		
			
		if (refList.contains("Person11")) {
			System.out.println("Person11 - object found..");
		} else {
			System.out.println("Person11 - object didn't found");
		}
		
		System.out.println(refList.isEmpty());
		System.out.println("Size of ArrayList : "+refList.size());
		System.out.println("Using get method : "+refList.get(0));
		
		
		Person refPerson = new Person();
		refPerson.setName("Person-1");

		refList.add(refPerson);
		
		System.out.println("Size of ArrayList : "+refList.size());
	
		// Retrieve all the values by using foreach from list
		System.out.println("Retrieve all the values by using for loop..");
		for (Object temp : refList) {
			System.out.println(temp);
		}
		
		
		System.out.println("Retrieve all the values by using Iterator Interface..");
		// Retrieve all the values by using Iterator
		Iterator refIterator = refList.listIterator();
		
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
	}

}


class Person{
	private String name;
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}