package day20;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class DependentService1 implements Runnable{

	private CountDownLatch refCountDownLatch =null;
	
	private CyclicBarrier refCyclicBarrier = null;
	
	volatile int count=0;	// avoid memory consistency errors in multi-threaded programs
							// value will always be read from the main memory instead of temporary registers (buffer)
	
	private AtomicInteger number = new AtomicInteger(0);
	
	public DependentService1(CyclicBarrier refCyclicBarrier) {
		this.refCyclicBarrier = refCyclicBarrier;
	}
	
	public void increment() {
		count = count+1;
	}
	
	public int getCount() {
		return count;
	}
	
	public int getNumber() {
		return number.incrementAndGet();
	}
	
	@Override
	public void run() {
		increment();	// calling line 19
		System.out.println("Count : "+getCount());	// calling line 23
		System.out.println("Number : "+getNumber());	// calling line 27
		// refCountDownLatch.countDown(); // Decrements the count of the latch, releasing all waiting threads if the count reaches zero. 
	} // end of run()
	
} // end of DependentService
