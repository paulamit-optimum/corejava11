package day20;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


public class Example67 {

	public static void main(String[] args) {
		
		ExecutorService refExecutorService = Executors.newFixedThreadPool(4);
		
		try { 
			//	CountDownLatch refCountDownLatch = new CountDownLatch(3); // calling the constrcutor, creates and object of Sync
			
				CyclicBarrier refCyclicBarrier = new CyclicBarrier(1);
				DependentService1 refDependentService1 = new DependentService1(refCyclicBarrier);
		
				for (int i = 0; i < 5; i++) {
					refExecutorService.submit(()->refDependentService1.run());
					
					// When multiple threads try to read and write a shared variable concurrently, 
					// and these read and write operations overlap in execution, 
					// then the final outcome depends on the order in which the reads and writes take place, which is unpredictable. 
					// This phenomenon is called Race condition
					
				}
				
				try {
					refCyclicBarrier.await();
				} catch (BrokenBarrierException e) {
					System.out.println("BrokenBarrierException Handled..");
				} // it will wait until latch count is zero
			 
			 	System.out.println("All dependent Services Initialized..");
			 
			} // end of try
		catch (InterruptedException e) {
				System.out.println("InterruptedException Handled Successfully.."); 
		} // end of catch
		finally{ 
			System.out.println("Closing files.."); 
		} // end of finally
			
	} // end of main

} // end of class
