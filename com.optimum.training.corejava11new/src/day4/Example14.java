package day4;

// concept ==> Method Overriding or dynamic polymorphism

class Laptop{
	void getDetails1() {
		System.out.println("I am in Laptop ==> getDetails1() ");
	}
	static void getDetails2() {
		System.out.println("I am in Laptop ==> getDetails2() ");
	}
} // end of Laptop

class OmenHP extends Laptop{
	@Override
	void getDetails1() {
		System.out.println("I am in OmenHP ==> getDetails1() ");
	}
	//@Override
	static void getDetails2() {
		System.out.println("I am in OmenHP ==> getDetails2() ");
	}
} // end of OmenHP

public class Example14 {
	public static void main(String[] args) {
		
	//	Laptop refLaptop = new OmenHP();
	//	refLaptop.getDetails1();
	//	refLaptop.getDetails2();
		
		OmenHP refOmenHP = (OmenHP) new Laptop();
		
		refOmenHP.getDetails1();
		refOmenHP.getDetails2();
		
		
	} // end of main
} // end of Example14
