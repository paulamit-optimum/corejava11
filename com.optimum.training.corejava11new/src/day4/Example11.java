package day4;

// concept ==> Constructor

class User{

	int number=2;
	
	User(){ 					// default constructor
		number = 10;
		System.out.println(number);
	} // end of constructor
	
	
	User(int number, String name){
		System.out.println(number + " "+name);
	} // end of constructor
	
	User(boolean data,long digit){
		System.out.println(data + " "+digit);
	}
} // end of User

public class Example11 {
	public static void main(String[] args) {
		new User(); 			// calls ==> line 9
		new User(50,"test1");	// calls ==> line 15
		new User(true,5555);    // calls ==> line 19
		
		
	} // end of main() 
} // end of Example11
