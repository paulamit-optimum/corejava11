package day27;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;



// Phone Book Entry Application using HashMap

public class Example94 {

	public static void main(String[] args) throws NumberFormatException, IOException {
		
		HashMap<String,Long> refHashMap = new HashMap<String,Long>();
		
		String name, mobile;  	// key
		Long phoneNumber;	// value
		
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
		System.out.println("1. Enter Phone Entries : ");
		System.out.println("2. lookup in the Phone Book : ");
		System.out.println("3. Display name in the Book : ");
		System.out.println("4. Exit");
		
		System.out.println("Enter your choice : ");
		int choice = Integer.parseInt(refBufferedReader.readLine());
		
		switch (choice) {
		case 1:
			
			System.out.println("Enter Name : ");
			name = refBufferedReader.readLine();
			
			System.out.println("Enter Mobile Number : ");
			mobile = refBufferedReader.readLine();
			phoneNumber = new Long(mobile);
			refHashMap.put(name, phoneNumber);		
			break;

		case 2:
			
			System.out.println("Enter Name : ");
			name = refBufferedReader.readLine();
			
			name = name.trim(); // remove unwanted spaces
			phoneNumber = refHashMap.get(name);
			
			System.out.println("Phone Number "+phoneNumber);
		
			break;
			
		case 3:
			Set<String> refSet = new HashSet<String>();
			refSet = refHashMap.keySet();
			
			System.out.println(refSet + "  "+refHashMap.values());
			
			break;
		}
		}
		
	}

}
