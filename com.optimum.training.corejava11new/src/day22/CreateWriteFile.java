package day22;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CreateWriteFile {
	
	public static void createWriteFile() {
		
		// step 1:
		Scanner sc = new Scanner(System.in);
		System.out.println("Write something to the file :");
		String data = sc.nextLine(); 
		
		// step 2:
		FileWriter refFileWriter = null;
		
		try {
			refFileWriter = new FileWriter("demo.txt");		// relative path
			
			refFileWriter.write("writing from the code");
			
			// step 3:
			for (int i = 0; i < data.length(); i++) {
				refFileWriter.write(data.charAt(i));
			} // end of for loop
			
		} catch (IOException e) {
			
			System.out.println("exception handled while writing to the file");
		}
		
		finally {
			try {
				refFileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception handled while closing to the file..");
			}
		}
		
			
	}

}
