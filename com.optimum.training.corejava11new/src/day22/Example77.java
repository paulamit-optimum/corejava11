package day22;

import java.util.Arrays;
import java.util.List;

public class Example77 {

	public static void main(String[] args) {
		
		List<String> refList = null;
		
		// old way of writing code
		refList = Arrays.asList("Paris","Tokyo","London");
		
		for (String string : refList) {
			System.out.println(string);
		}
		
		
		// new way of writing code
		refList = Arrays.asList("Paris","Tokyo","London");
		
		System.out.println("\nusing forEach and lamda");
		refList.forEach(result -> System.out.println(result));
 
		System.out.println("\nusing method reference..");
		refList.forEach(System.out::println);
	
		
		
		
	}

}
