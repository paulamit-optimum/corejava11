package day22;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Example82 {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Hongkong", "Tokyo", "Singapore","Dubai","France");
		
		String refString1 = list.stream().collect(StringBuilder::new,
		                            (sb, s1) -> sb.append(" ").append(s1),
		                            (sb1, sb2) -> sb1.append(sb2.toString())).toString();
		  System.out.println(refString1);
		
		  IntStream stream = IntStream.range(1, 100);
		
		  List<Integer> refList = stream.parallel()
				  						.filter(i -> i % 10 == 0)
				  						.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
		  System.out.println(refList);
		  
		  
		  String refString2 = Stream.of("Java","Python","Scala")
				  			 .parallel()
				  			 .unordered()
				  			 .collect(new MyCollector());
		  System.out.println(refString2);

		  
	} // end of main
	
	private static class MyCollector implements Collector<String, StringBuffer,String>{
		
		@Override
		public Supplier<StringBuffer> supplier() {
			return () -> {
	               System.out.println("inside supplier");
	               return new StringBuffer();
	           }; 
		} // end of supplier
		
		@Override
		public BiConsumer<StringBuffer, String> accumulator() {
			return (sb,s) ->{
				System.out.println(System.identityHashCode(sb)
                      + " thread: "
                      + Thread.currentThread().getName()
                      + ", processing: " + s);
						sb.append(" ").append(s);
  		};
		} // end of BiConsumer
	
		@Override
		public BinaryOperator<StringBuffer> combiner() {
			return (stringBuilder, s) -> {
	               System.out.println("combiner function call");
	               return stringBuilder.append(s);
	           };
		} // end of BinaryOperator
		
		@Override
		public Function<StringBuffer, String> finisher() {
			
			return stringBuilder -> stringBuilder.toString();
		}
		
		@Override
		public Set<Characteristics> characteristics() {
			return EnumSet.of(Collector.Characteristics.CONCURRENT);
		}
	}
	
}
