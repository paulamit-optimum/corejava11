package day22;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Example84 {

	public static void main(String[] args) {
		
		// peek() method returns a new Stream consisting of all the elements from the 
		// original Stream after applying a given Consumer action
		
		// peek() method is an intermediate Stream operation. 
		// to process the Stream elemenet through peek(), we must use terminal operation
		
		List<Integer> refList = Arrays.asList(10,20,30,40,50);
		
		List<Integer> newList = refList.stream()
						.peek(System.out::println)
						.collect(Collectors.toList());
		System.out.println(newList);
		
		Stream.of("Java", "Python", "Scala", "React JS", "Angular")
        .filter(e -> e.length() > 3)
        .peek(e -> System.out.println("Filtered value: " + e))
        .map(String::toUpperCase)
        .peek(e -> System.out.println("Mapped value: " + e))
        .collect(Collectors.toList());
	

	}

}
