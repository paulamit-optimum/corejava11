package day22;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Example78 {

	public static void main(String[] args) {
		
		// old way of wrting code
		List<Integer> refList = Arrays.asList(10, 20, 30, 40, 50, 60, 70);

		int count1 = 0;
		
		for (Integer integer : refList) {
			if(integer % 2 ==0) {
				count1++;
			} // end of if
		} // end of for
		
		System.out.println(count1);
		
		
		// by using Stream API
		System.out.println("\nby using Java Stream API..");
		int count2 = (int) refList.stream().filter(integer -> integer%2 ==0).count();
		System.out.println(count2);
		
		List<Integer> result = refList.stream().filter(integer -> integer%2 ==0)
				.collect(Collectors.toList());	// toList is a factory method / factory design pattern
		
		System.out.println(result);
	
		
	}

}
