package day22;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {
	
	public static void redFromFile() {
		FileReader refFileReader = null;
		int data;
		
		try {
			refFileReader = new FileReader("demo.txt");
			
			try {
				while((data=refFileReader.read())!=-1){
					System.out.print((char)data);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// we need a while loop to read the file
		
		finally {
			try {
				refFileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
