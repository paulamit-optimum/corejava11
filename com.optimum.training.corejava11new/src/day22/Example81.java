package day22;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Example81 {

	public static void main(String[] args) {
		
		int[] number = {10, 20, 30, 40, 50};
	
		System.out.printf("Source", Arrays.toString(number));
		
		System.out.println("Even numbers : ");
		
		runWithoutLimit(Arrays.stream(number));
		runWithLimit(Arrays.stream(number));
		
	} // end of main()

	private static void runWithoutLimit(IntStream refIntStream) {
		
		System.out.println("running without limit()..");
		
		// filter even numbers
		refIntStream.filter(i->i%2 == 0).forEach(System.out::println);
		
	} // end of runWithoutLimit
	
	private static void runWithLimit(IntStream refIntStream) {
		
		System.out.println("Running with limit 3 ");
		
		// filter even numbers
		refIntStream.filter(i->i%2 == 0).limit(3).forEach(System.out::println);
		
	} // end of runWithLimit

}
