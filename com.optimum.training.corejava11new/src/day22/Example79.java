package day22;

import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class Example79 {

	public static void main(String[] args) {
		
		long number1 = Stream.of("Java", "Python", "R","Kotlin","Swift").count();
		System.out.println(number1); // 3
		
		long number2 = Stream.of("Java", "Python", "R","Kotlin","Swift")
                		.mapToLong(s -> 2L)
                		.reduce(0, Long::sum);
		System.out.println(number2); // 3

		double sum = DoubleStream.of(1.1,1.5,2.5,5.4)
						.reduce(0, Double::sum);
		System.out.println(sum); // 10.5


	}

}
