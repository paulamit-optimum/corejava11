package day22;

import java.util.Arrays;
import java.util.List;

public class Example80 {

	public static void main(String[] args) {
		
			List<String> listRef = Arrays.asList("Hello","A","B","C","D","E");
		
		   System.out.println("Sequential Stream Example : ");
		   listRef.stream().forEach(System.out::print); // sequential stream
		  
		   System.out.println("\nParallel Stream Example : ");
		   listRef.parallelStream().forEach(System.out::print); // parallelStream
		

	}

}
