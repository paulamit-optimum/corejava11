package day22;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee{
	int employeeID;
	String employeeName;
	long employeeSalary;
	
	public Employee(int employeeID,String employeeName,long employeeSalary) {
		this.employeeID=employeeID;
		this.employeeName=employeeName;
		this.employeeSalary=employeeSalary;
	} 
} // end of Employee class


public class Example83 {

	public static void main(String[] args) {
		List<Employee> refList = new ArrayList<Employee>();
		refList.add(new Employee(1,"Employee 1",11111));
		refList.add(new Employee(2,"Employee 2",22222));
		refList.add(new Employee(3,"Employee 3",33333));
		refList.add(new Employee(4,"Employee 4",44444));
		refList.add(new Employee(5,"Employee 5",55555));
		// using filter method
		List<Long> empFilteredList = refList.stream()
				.filter(e -> e.employeeSalary>30000) // filter data
				.map(e->e.employeeSalary) // fetching price
				.collect(Collectors.toList()); // collecting as List
		 refList.stream()
		.filter(e->e.employeeSalary>30000) // filtering data
		.forEach(e->System.out.println(e.employeeName));
		System.out.println(empFilteredList);
		// List to Map
		Map<Integer,String> mapRef = refList.stream()
				.collect(Collectors.toMap(e->e.employeeID, e->e.employeeName));
		System.out.println(mapRef);
		// max() method to get maximum Employee Salary
		Employee refEmployee1 = refList.stream()
				.max((emp1,emp2)->emp1.employeeSalary>emp2.employeeSalary?1:-1).get();
		System.out.println("Maximum"+refEmployee1.employeeSalary);
		// min() method to get minimum Employee Salary
		Employee refEmployee2 = refList.stream()
				.min((emp1,emp2)->emp1.employeeSalary>emp2.employeeSalary?1:-1).get();
		System.out.println("Minimum"+refEmployee2.employeeSalary);

	}

}
