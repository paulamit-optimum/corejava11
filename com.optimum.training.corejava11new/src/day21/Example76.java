package day21;

import java.util.HashMap;
import java.util.Map;

public class Example76 {

	public static void main(String[] args) {
		
		Map<Integer,String> refMap = new HashMap();
		refMap.put(444,"value-4");
		refMap.put(441,"value-1");
		refMap.put(443,"value-3");

		System.out.println(refMap);
		
		// by using merge()
		String result = refMap.merge(555,"value-10", (k,v)-> k + v);
		
		System.out.println("\nmerged value : "+result);
		
		// updated Map
		System.out.println("\nRetrieving data by using entrySet");
		
		for(Map.Entry ref : refMap.entrySet() ) {
			System.out.println(ref.getKey()+ " "+ref.getValue());
		}
	
	}

}
