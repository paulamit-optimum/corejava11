package day21;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Example73 {

	public static void main(String[] args) {
		
//		Set refSet = new HashSet();  // reverse order
//		Set refSet = new LinkedHashSet();	// it will keep the same order
		Set<String> refSet = new TreeSet();		// ClassCastException at runtime
		refSet.add("Hello-1");
		refSet.add("Hello-10");
		refSet.add("Hello-5");
		refSet.add("Hello-1");
//		refSet.add(12345);
//		refSet.add(52345);
//		refSet.add(32345);
		
		System.out.println(refSet);
		
		System.out.println("\nBy Using for each loop..");
		for (Object object : refSet) {
			System.out.println(object);
		}
		
		System.out.println("\nBy Using Iterator interface..");
		Iterator refIterator = refSet.iterator();
		
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
	
		
		System.out.println("\nBy using forEach and lamda expression..");
		
		refSet.forEach(value->{
			System.out.println(value);
		});
		
		
	}

}
