package day21;

import java.util.HashMap;
import java.util.Map;

// Map interface 

public class Example75 {

	public static void main(String[] args) {
		
		Map<Integer,String> refMap = new HashMap();
		refMap.put(501,"d-value-1");
		refMap.put(301,"a-value-1");
		refMap.put(101,"f-value-1");
		refMap.put(201,"e-value-1");
		refMap.put(401,"c-value-1");
		refMap.put(101,"z-value-1");
		
		System.out.println("\nRetrieving data by using entrySet");
		
		for(Map.Entry ref : refMap.entrySet() ) {
			System.out.println(ref.getKey()+ " "+ref.getValue());
		}
		
		System.out.println("\nRetrieving data by using keySet");
		System.out.println(refMap.keySet());
		
		System.out.println("\nRetrieving data by using forEach and lamda..");
		refMap.forEach((key,value)->System.out.println(value));
		
		System.out.println("\nBy using entrySet().forEach and lamda..");
		refMap.entrySet().forEach(ref->System.out.println(ref.getKey() + " "+ref.getValue()));
	
		
	}

}
