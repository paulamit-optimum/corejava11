package day21;

public class Product implements Comparable<Product>{

	private int productID;
	private String productName;
	
	
	public Product(int i, String string) {
		productID = i;
		productName = string;
	}

	

	public int getProductID() {
		return productID;
	}



	public void setProductID(int productID) {
		this.productID = productID;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return productID + " "+productName;
	}

	@Override
	public int compareTo(Product refProduct) {
		
		return productID - refProduct.productID; // ascending order
	}

}
