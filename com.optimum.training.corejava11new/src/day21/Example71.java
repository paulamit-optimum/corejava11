package day21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import day20.Customer;


// Sorting by using Comparator Interface
public class Example71 {

	public static void main(String[] args) {
		
		Iterator refIterator = null;
		
		var refList = new ArrayList();
		
		refList.add(new Customer(105,"Customer-5"));
		refList.add(new Customer(102,"Customer-2"));
		refList.add(new Customer(101,"Customer-1"));
		refList.add(new Customer(106,"Customer-6"));
		refList.add(new Customer(103,"Customer-3"));
		refList.add(new Customer(101,"Customer-1"));
		
		System.out.println("\nBefor Sorting..");
		refIterator = refList.listIterator();
		
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
		System.out.println("\nUsing for each loop");
		for (Object object : refList) {
			System.out.println(object);
		}
		
		// Sorting ==> by using lamda expression
		
//		Comparator<Customer> refComparator = (c1, c2) -> c1.getCustomerID() - c2.getCustomerID();
		Comparator<Customer> refComparator = (c1, c2) -> c1.getCustomerName().compareTo(c2.getCustomerName());
		Collections.sort(refList,refComparator);
		
		System.out.println("\nAfter Sorting using lamda expression..");
		refIterator = refList.listIterator();
		
		while(refIterator.hasNext()) {
			System.out.println(refIterator.next());
		}
		
		
		// :: Operator or Method Reference Operator
		System.out.println("\nBy using :: Operator");
		refList.sort(Comparator.comparing(Customer::getCustomerName));
		
		for (Object object : refList) {
			System.out.println(object);
		}
		
		System.out.println("\nReverse Order : ");
		refList.sort(refComparator.reversed());
		
		for (Object object : refList) {
			System.out.println(object);
		}
	}

}








