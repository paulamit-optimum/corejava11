package day21;

import java.util.ArrayList;
import java.util.Collections;

// Sorting by Using Comparable Interface

public class Example72 {

	public static void main(String[] args) {
		
		var refList = new ArrayList();
		refList.add(new Product(505,"Product-5"));
		refList.add(new Product(503,"Product-3"));
		refList.add(new Product(501,"Product-1"));
		refList.add(new Product(502,"Product-2"));
		refList.add(new Product(504,"Product-4"));

		System.out.println("\nBefore Sorting..");
		for (Object object : refList) {
			System.out.println(object);
		}
		
		// Sorting by using Comparable interface
		Collections.sort(refList);
		
		System.out.println("\nAfter Sorting..");
		for (Object object : refList) {
			System.out.println(object);
		}
	}

}
