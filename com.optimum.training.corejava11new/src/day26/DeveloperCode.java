package day26;

// code coverage
public class DeveloperCode {

	public static void main(String[] args) {
		
		System.out.println("Code Coverage is executing..");
		
		System.out.println(method1());
		System.out.println(method2());
	
	} // end of main
	
	public static String method1() {
		String refString = "method1 running..";
		return refString;
	} // method1()
	
	public static String method2() {
		String refString = "method2 running..";
		return refString;
	
	} // method2()

}
