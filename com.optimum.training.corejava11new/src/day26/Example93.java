package day26;

import org.junit.Test;

import junit.framework.Assert;

public class Example93 {
	
	@Test
	public void test1() {
		Assert.assertEquals(DeveloperCode.method1(),"start");
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(DeveloperCode.method2(),"stop");
	}

}
