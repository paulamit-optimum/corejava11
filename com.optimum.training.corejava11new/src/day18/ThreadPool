Thread Pools in Java
--------------------
    (Ideal way to manage the number of tasks executing concurrently)
    
    - A thread pool in Java is a common multithreaded design pattern. 
    - It is a collection of several worker threads that waits for tasks to perform. 
    - It contains a queue that keeps tasks waiting to get executed.

	- In other words, a thread pool is basically a group of threads that are used to 
	  perform multiple tasks in the background simultaneously.

	- A thread pool may contain a number of threads where each thread will perform a specific job. 
	  When a task is sent to the thread pool, one of the available worker threads process it.
	
	- If all worker threads in the thread pool are busy in performing their tasks, 
	  a new task that needs to be processed, either additional worker threads are created 
	  to handle incoming tasks, or tasks are wait in a queue until worker threads are available 
	  or free.
	
	- When a thread in the thread pool completes the processing of task, 
	  it waits in a queue for performing another task. 
	  
	  In this way, thread in the thread pool can be reused for doing different tasks. 
	  The re-usability of thread helps to avoid creating a new thread for every task.
	
	
	Concurrency API Executors Framework
	-----------------------------------
		ExecutorService
		Executors 
		ThreadPoolExecutor

	- Executor interface and ExecutorService sub interface
	- Executors helper class
	- ThreadPoolExecutor extends AbstractExecutorService extends Object implements ExecutorService
	- interface ExecutorService extends Executor
	- interface Executor
	
	Thread Pool manages the pool of worker threads. 
	It contains a queue that keeps tasks waiting to get executed.
	We can use ThreadPoolExecutor to create thread pool in Java.
	
	Java thread pool manages the collection of Runnabble threads.
	The worker threads executes Runnable threads from the queue.
	
	1. Single Thread Pool
	2. Fixed Thread Pool
	3. Cached Thread Pool

	Use of Thread Pool in Java
	--------------------------
	1. A Thread pool is used to avoid creating a new thread to perform each task.
	2. It can be used to execute tasks efficiently.
	3. The realtime use of Thread pool is in Servlet and JSP where the container 
	   creates a thread pool to process requests.
	
	Advantage of Thread Pool in Java
	--------------------------------
	1. Thread pool saves time and gives better performance because there is no need to create 
	   new thread for other tasks that are waiting in a queue.

	2. It enables a loosely coupled design by decoupling the creation and execution of tasks.

	We know that a process can create only a limited number of threads. 
	Creating so many threads can be expensive in terms of time and resources. 
	If we create a thread at the time of request processing, it will slow down response time.
	
	To overcome these issues, a pool of thread is created during the start-up of application, 
	and threads are reused for request processing. 
	This pool of thread is known as thread pool in Java and threads are known as worker threads.
