package day23;

 //@JavaAttraction class OOP { }  					// doesn't compile
/*
 class OOP{
	@JavaAttraction String polymorphism1() {
		return (@JavaAttraction String) "many form";	// doesn't compile
	}
	
	@JavaAttraction OOP(@JavaAttraction String refString){		// doesn't compile
		super();
	}
	
	@JavaAttraction int developerID;					
 }
*/