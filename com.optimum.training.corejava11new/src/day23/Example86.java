package day23;

/*
assert is a Java keyword used to define an assert statement. 
An assert statement is used to declare an expected boolean condition in a program. 
If the program is running with assertions enabled, then the condition is checked at runtime. 
If the condition is false, the Java runtime system throws an AssertionError .
*/

// assert expression1;	
// assert expression1 : expression2; 

	// [ expression1 must be a boolean expression]
	// [expression2 must return a value (must not return void)]

// True / False	 ==> Boolean


public class Example86 {

	public static void main(String[] commandLineArgs) {
		
		int value = Integer.parseInt(commandLineArgs[0]);
		
		assert value <=5;
		
		System.out.println("Success");
	}
}
