package day23;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class MyClassSerialization implements Serializable{
	String name;
	String city;  // static 
	int mobile;   // transient
	MyClassSerialization refMyClassSerialization;
	ObjectOutputStream refObjectOutputStream;
	FileOutputStream refFileOutputStream;
	
	void myMethod() {
			refMyClassSerialization = new MyClassSerialization();
			refMyClassSerialization.name = "Person-1";
			refMyClassSerialization.city = "Singapore";
			refMyClassSerialization.mobile = 54324567;
			
			try {
				refFileOutputStream = new FileOutputStream("customer.txt"); // customer.doc / customer.html
				refObjectOutputStream = new ObjectOutputStream(refFileOutputStream);
				refObjectOutputStream.writeObject(refMyClassSerialization);
			} // end of try 
			
			catch (Exception e) {
				e.printStackTrace();
			} // end of catch
			
			finally {
				try {
					System.out.println("closing file..");
					refFileOutputStream.close();
					refObjectOutputStream.close();
					} // end of try 
				catch (IOException e) {
					e.printStackTrace();
				} // end of catch
			} // end of finally
	} // end of MyMethod
} // end of class


// Serialization
public class Example89 {

	public static void main(String[] args) {
		
		// create an object an MyClassSerialization 
		MyClassSerialization refMyClassSerialization = new MyClassSerialization();
		refMyClassSerialization.myMethod(); // call the method of myMethod()
				

	}

}
