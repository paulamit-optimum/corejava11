package day23;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD,ElementType.CONSTRUCTOR,ElementType.FIELD})
public @interface JavaAttraction {

}
