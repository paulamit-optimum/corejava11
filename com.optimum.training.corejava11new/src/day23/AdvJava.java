package day23;

class Language{ }

enum Data { publicData, privateData}

public @interface AdvJava {
	String patterns();					// protected, private, final ==> error
	String[] refString();
//	String[][] refString2();			// error
//	Integer getInteger();				// error
	Data getData() default Data.publicData;
	Java practice() default @Java(learnPerDay=2);
}

// note: 

//	primitive types (example: int and long does support) 
//  wrapper classes (example: Integer and Long doesn't support) 
