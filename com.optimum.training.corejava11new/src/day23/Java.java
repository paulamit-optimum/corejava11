package day23;

// marker annotation, since it does not contain any elements
public @interface Java {
	int learnPerDay();	// annotation element is an attribute that stores values
}						// about the particular usage of an annotation


@interface Java1{
	
}

class A{
	@Override
	public String toString() {
		return super.toString();
	}
}