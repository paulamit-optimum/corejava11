package day23;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

class DeSerializationDemo {
	MyClassSerialization refSerializationDemo;
	FileInputStream refFileInputStream;
	ObjectInputStream refObjectInputStream;
	void myMethod() {
		try {
			refFileInputStream = new FileInputStream("customer.txt");
			refObjectInputStream = new ObjectInputStream(refFileInputStream);
			
			refSerializationDemo = (MyClassSerialization) refObjectInputStream.readObject();  // down casting
			
			// print on console
			System.out.println("Customer Name : "+refSerializationDemo.name);
			System.out.println("Customer City : "+refSerializationDemo.city);
			System.out.println("Customer Mobile : "+refSerializationDemo.mobile);
		} // end of try 
		catch (Exception e) {
			e.printStackTrace();
		} // end of catch
		finally {
			try {
				refFileInputStream.close();
				refObjectInputStream.close();
			} // end of try
			catch (IOException e) {
				e.printStackTrace();
			} // end of catch
		} // end of finally
	} // end of myMethod
}
// DeSerialization
public class Example90 {

	public static void main(String[] args) {
		
		// create of object of DeSerialization
		DeSerializationDemo refDeSerializationDemo = new DeSerializationDemo();
		refDeSerializationDemo.myMethod(); // call the myMethod


	}

}
