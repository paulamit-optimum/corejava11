package day23;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Example88 {

	public static void main(String[] args) {
		
		// Localization ==> Date and Time
		
		// Internationalization ==> Date and Time

		// May-05-2021	[DD-May-YYYY] / [MM-DD-YYYY]
		
		var refDate = LocalDate.of(2021,Month.MAY, 21);
		
		System.out.println(refDate.getDayOfWeek());
		
		var refLocalTime = LocalTime.of(11, 05, 59);
		
		System.out.println(refLocalTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
		System.out.println(refDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
		
		var refVar1 = NumberFormat.getInstance(Locale.GERMANY);
		System.out.println(refVar1.format(2000));
	}

}
