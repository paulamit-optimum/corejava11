package day19;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class ATM {
	int availableBalance = 0;
	
	void depositAmount(int amount) {		// later you can change to synchronized method
		availableBalance=amount;
	}
	
	void withdrawalAmount(int amount) {
		availableBalance=amount;
	}
	
	// write a logic for available balance
	// withdrawalAmount should be lesses than availableBalance
	// after deposit the amount show the balance
	// after withdrawalAmount the amount show the balance
}

public class Example66 {

	public static void main(String[] args) {
		
		var refThread = Executors.newScheduledThreadPool(5);

		ATM refATM = new ATM();
		
		refThread.submit(()->refATM.depositAmount(10));
		refThread.submit(()->refATM.withdrawalAmount(8));
		
		System.out.println(refATM.availableBalance);
		
	//	refThread.shutdown();
	//	refThread.shutdownNow();
				
	/*	try {
			refThread.awaitTermination(5,TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	*/	
		
		
		// take user input
	}

}
